import React, { Component } from 'react';
import './folderList.css';
import { Row, Col, Navbar, Card, CardGroup, CardColumns, CardDeck, Modal, Button } from 'react-bootstrap';
import { faTrashAlt } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Config } from '../../conf/Config'

class folderList extends Component {
    constructor() {
        super();
        this.state = {
            content: [],
            updatedPath: "",
            fileName: "",
            modalVisibility: false
        }

        this.getContent = this.getContent.bind(this);
        this.updatePath = this.updatePath.bind(this);
        this.toggleModal = this.toggleModal.bind(this)
        this.deleteFile = this.deleteFile.bind(this)
    }

    componentDidMount() {
        this.getContent();
    }

    getContent() {
        fetch(( Config.apiPath + 'browser/' + this.state.updatedPath))
            .then(res => res.json())
            .then(res => {
                this.setState({ content: res })
            })
    }

    downloadFile(fileName) {
        window.location.replace(Config.apiPath + 'download/' + fileName + '/' + this.state.updatedPath);
    }

    updatePath(path, isDirectory) {
        if (isDirectory === true) {
            this.props.callbackFromParent(path);
        } else {
            this.downloadFile(path)
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ updatedPath: nextProps.updatedPath.split('/').join('*') }, () => {
            this.getContent();
        })
    }

    toggleModal(fileName) {
        this.setState({modalVisibility: !this.state.modalVisibility, fileName: fileName})
    }

    deleteFile() {
        fetch(Config.apiPath + 'delete', {
        method: 'DELETE',
        body: JSON.stringify({ file: this.state.fileName, path: this.state.updatedPath }),
        headers: {
            'Content-Type': 'application/json'
        }
        })
        .then(()=> {
            this.toggleModal('');
            this.getContent();
        })
        .catch((err) => {
            console.log(err)
        })
    }

    render() {
        return (
            <Row>
                <Modal show={this.state.modalVisibility}>
                    <Modal.Header closeButton>
                        <Modal.Title>Delete File</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        You're going to delete the file: <strong> {this.state.fileName} </strong>
                        </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={() => {this.toggleModal('')}}>
                            Cancel
                        </Button>
                        <Button variant="danger" onClick={this.deleteFile}>
                            Delete
                        </Button>
                    </Modal.Footer>
                </Modal>
                {this.state.content.map((element, index) => {
                    let imgPath = element.isDirectory ? "img/folder.svg" : "img/images.svg"

                    return (
                        <Col key={index} sm={3}>
                            <Card className="content-card">
                                <Card.Img variant="top" src={imgPath} onDoubleClick={() => { this.updatePath(element.fileName, element.isDirectory) }} />
                                <Card.Body className="text-center">
                                    <Card.Title>
                                        {element.fileName}
                                        {!element.isDirectory &&
                                            <FontAwesomeIcon icon={faTrashAlt} className="trash-icon" size="xs" onClick={() => {this.toggleModal(element.fileName)}} />
                                        }
                                    </Card.Title>
                                </Card.Body>
                            </Card>
                        </Col>
                    )
                })}
            </Row>
        )
    }
}

export default folderList;