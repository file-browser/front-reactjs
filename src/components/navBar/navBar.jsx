import React, { Component } from 'react';
import logo from './logo.svg';
import './navBar.css';
import { Row, Col, Navbar, InputGroup, Form, FormControl, Button, OverlayTrigger, Popover, Image } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCaretLeft, faCloudUploadAlt } from '@fortawesome/free-solid-svg-icons'
import Dropzone from 'react-dropzone'
import { Config } from '../../conf/Config'

class navBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            displayedPath: "",
            file: []
        }

        this.back = this.back.bind(this)
        this.sendFile = this.sendFile.bind(this)
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ displayedPath: nextProps.updatedPath });
    }

    back() {
        var array = this.state.displayedPath.split('/')
        var partToRemove = array.length - 1

        this.props.callbackFromParent(array[partToRemove]);
    }

    sendFile() {
        var formData = new FormData()
        
        formData.append('file', this.state.file)

        fetch(Config.apiPath + 'upload/' + this.state.displayedPath.split('/').join('*'), {
            method: 'POST',
            body: formData
        })
        .then(() => {
            this.setState({file: []})
            this.props.callbackFromParent();
        })
    }

    render() {
        var popover = (
            <Popover id="upload-popover" className="popover-width">
                <Popover.Content className="text-center">
                    <Dropzone onDrop={acceptedFiles => this.setState({file: acceptedFiles[0]})}>
                        {({ getRootProps, getInputProps }) => {
                            if(this.state.file.length == 0) {
                                return (
                                    <section>
                                        <div {...getRootProps()}>
                                            <input {...getInputProps()} />
                                            <Image src="img/uploaded.svg" fluid />
                                            <strong>Drag your file here</strong>
                                        </div>
                                    </section>
                                )
                            } else {
                                return (
                                    <section>
                                        <div {...getRootProps()}>
                                            <input {...getInputProps()} />
                                            <Image src="img/uploading.svg" fluid />
                                            <strong>{this.state.file.name}</strong>
                                        </div>
                                    </section>
                                )
                            }
                        }}
                    </Dropzone>
                    <Button variant="primary" type="submit" onClick={this.sendFile.bind(this)} disabled={this.state.file.length == 0}>
                        Upload
                    </Button>
                </Popover.Content>
            </Popover>
        );
        
        return (
            <Navbar className="color" variant="dark" as={Row}>
                <Col sm="2">
                    <Navbar.Brand href="#">
                        <img
                            alt=""
                            src={logo}
                            width="30"
                            height="30"
                            className="d-inline-block align-top"
                        />{' '}
                        File Browser</Navbar.Brand>
                </Col>
                <Col sm="8">
                    <InputGroup>
                        <InputGroup.Prepend>
                            <InputGroup.Text id="pathInput" as={Button} onClick={this.back}>
                                <FontAwesomeIcon icon={faCaretLeft} size="lg" color="green" />
                            </InputGroup.Text>
                        </InputGroup.Prepend>
                        <FormControl
                            placeholder={".." + this.state.displayedPath}
                            aria-label="pathInput"
                            aria-describedby="pathInput"
                            readOnly
                        />
                    </InputGroup>
                </Col>
                <Col sm="2">
                    <OverlayTrigger trigger="click" placement="bottom" overlay={popover}>
                        <Button variant="secondary">
                            <FontAwesomeIcon className="margin-right" icon={faCloudUploadAlt} color="white" />
                            Upload File</Button>
                    </OverlayTrigger>
                </Col>
            </Navbar>
        )
    }
}

export default navBar;