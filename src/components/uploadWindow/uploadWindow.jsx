import React, {Component} from 'react';
import './uploadWindow.css';
import {Row, Col, Navbar, Card, Accordion, Button} from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronRight, faChevronLeft } from '@fortawesome/free-solid-svg-icons'

class uploadWindow extends Component { 
    chevronIcon =  faChevronLeft;
    
    render() {
        return( 
            <Accordion defaultActiveKey="0">
            <Card>
                <Accordion.Toggle as={Button} variant="link" eventKey="0" onClick={this.switchIcon()}>
                    Click me!
                    <FontAwesomeIcon icon={this.chevronIcon} />
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="0">
                    <Card.Body>Hello! I'm the body</Card.Body>
                </Accordion.Collapse>
            </Card>
          </Accordion>
        )
    }

    switchIcon() {
        console.log(this.chevronIcon);
        this.chevronIcon = this.chevronIcon == faChevronLeft ? faChevronRight : faChevronLeft;
    }
}

export default uploadWindow;