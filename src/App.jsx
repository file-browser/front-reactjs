import React, {Component} from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container } from 'react-bootstrap';

// importation des composants
import NavBar from './components/navBar/navBar';
import FolderList from './components/folderList/folderList';

class App extends Component {
  constructor (props) {
    super(props);

    this.state = {
      updatedPath: '',
    }

    this.addPart = this.addPart.bind(this)
    this.removePart = this.removePart.bind(this)
  }

  addPart(value) {
    this.setState({updatedPath: this.state.updatedPath + '/' + value});
  }

  removePart(value) {
    var newPath = this.state.updatedPath.replace('/' + value, '')
    
    this.setState({updatedPath: newPath});
  }

  render() {
    return (
      <div>
        <NavBar callbackFromParent={this.removePart} updatedPath={this.state.updatedPath}></NavBar>
        <Container fluid>
          <FolderList callbackFromParent={this.addPart} updatedPath={this.state.updatedPath}></FolderList>
        </Container>
      </div>
    );
  }
}

export default App;
