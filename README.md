# File Browser 

## Introduction
This application allows you to navigate into folders and manipulate files. 
You must launch the API first and configure the address. To do so edit the **Config.jsx** file located in the **src/conf** folder.
Fill in the **apiPath** property with the desired address.
**Like:**

    http://localhost:3001/
You can navigate trough folders as you would with a basic file explorer. Double click on a folder to navigate into. Double click on a file will download it. You can upload a file to the folder you are into by clicking the *upload file* button. Delete a file by clicking the trash icon next to its name.

## Get Started
### Prerequisites
**npm v10.5**

**nodejs**

### Launch
First execute in the root directory:

    npm install
Then to launch the application use:

    npm start
By default the application uses the *3000* port.